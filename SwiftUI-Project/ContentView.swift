import SwiftUI

struct ContentView: View {
    var body: some View {
        TabView {
            MyCards()
                .tabItem {
                    Image(systemName: "star.fill")
                    Text("Notes")
                    
            }
            Settings()
                .tabItem {
                    Image(systemName: "gear")
                    Text("Settings")
            }
//            Couses()
//                .tabItem {
//                    Image(systemName: "star.fill")
//                    Text("Courses")
//
//            }
        }
    }
    
    struct ContentView_Previews: PreviewProvider {
        static let viewModel = ListViewModel()
        static var previews: some View {
            Group {
                ContentView().environmentObject(viewModel)
                ContentView().environmentObject(viewModel)
                    .environment(\.colorScheme, .dark)
            }
        }
    }
}
