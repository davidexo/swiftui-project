//
//  Settings.swift
//  SwiftUI-Project
//
//  Created by David Bielenberg on 11.01.20.
//  Copyright © 2020 David Bielenberg. All rights reserved.
//

import SwiftUI

struct Settings: View {
    
    @State var receave = false
    @State var number = 1
    @State var selection = 1
    @State var date  = Date()
    @State var email = ""
    @State var submit = false
    
    
    var body: some View {
        NavigationView {
            Form {
                Section(header: Text("Notifications")) {
                    Toggle(isOn: $receave) {
                        Text("Receave Notifications")
                    }
                    Stepper(value: $number, in: 1...10) {
                        Text("\(number) Notification\(number > 1 ? "s" : "") per week")
                    }
                }
                Section(header: Text("Rest")) {
                    Picker(selection: $selection, label: Text("Favorite Course")) {
                        Text("SwiftUI").tag(1)
                        Text("React").tag(2)
                        Text("VueJS").tag(3)
                    }
                    DatePicker(selection: $date, label: { /*@START_MENU_TOKEN@*/Text("Date")/*@END_MENU_TOKEN@*/ })
                    TextField("Your Email", text: $email)
                        .textFieldStyle(RoundedBorderTextFieldStyle())
                }
                Button(action: { self.submit.toggle() } ){
                    Text("Submit")
                }
                .alert(isPresented: $submit) {
                    Alert(title: Text("Thanks"), message: Text("Your email: \(email)"))
                }
            }.navigationBarTitle("Settings")
        }
    }
}

struct Settings_Previews: PreviewProvider {
    static var previews: some View {
        Settings()
    }
}
