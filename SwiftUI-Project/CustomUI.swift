//
//  CustomUI.swift
//  SwiftUI-Project
//
//  Created by Davi/Users/david/Apps/SwiftUI-Project/SwiftUI-Project/CustomUI.swiftd Bielenberg on 20.12.19.
//  Copyright © 2019 David Bielenberg. All rights reserved.
//

import SwiftUI


struct ActionButtonStyle: ButtonStyle {
    
    let size = 80 as CGFloat
    @State var isActive: Bool = false
    
    @EnvironmentObject var viewModel: ListViewModel
    
    func makeBody(configuration: Self.Configuration) -> some View {
        configuration.label
            .frame(width: size, height: size)
            .font(.largeTitle)
            .foregroundColor(Color.white)
            .background(configuration.isPressed ? Color("Accent") : Color("blue"))
            .cornerRadius(size/2)
            .shadow(color: Color.black.opacity(0.3),
                    radius: 20, x: 0, y: 10)
            .animation(.easeOut)
            .rotationEffect(isActive ? Angle(degrees: 45.0) : Angle(degrees: 0.0))
//            .onTapGesture {
//                print("TappedYo!")
//                self.isActive = true
//                self.viewModel.togglePopUp()
//                print(self.isActive)
//        }
    }
}

struct PrimaryButtonStyle: ButtonStyle {
    
    func makeBody(configuration: Self.Configuration) -> some View {
        configuration.label
            .frame(height: 50)
            .padding(20.0)
            .font(.title)
            .foregroundColor(.white)
            .background(Color("blue"))
            .cornerRadius(8)
        
    }
}


struct CustomTextField: View {
    
    public struct CustomTextFieldStyle : TextFieldStyle {
        public func _body(configuration: TextField<Self._Label>) -> some View {
            configuration
                .font(.largeTitle) // set the inner Text Field Font
                .padding(10) // Set the inner Text Field Padding
                //Give it some style
                .background(
                    RoundedRectangle(cornerRadius: 5)
                        .strokeBorder(Color.primary.opacity(0.5), lineWidth: 3))
        }
    }
    @State private var password = ""
    @State private var username = ""
    
    var body: some View {
        VStack {
            TextField("Test", text: $username)
                .textFieldStyle(CustomTextFieldStyle()) // call the CustomTextField
            SecureField("Password", text: $password)
                .textFieldStyle(CustomTextFieldStyle())
        }.padding()
    }
}
