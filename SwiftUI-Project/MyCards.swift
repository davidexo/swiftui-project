//
//  MyCards.swift
//  SwiftUI-Project
//
//  Created by David Bielenberg on 21.12.19.
//  Copyright © 2019 David Bielenberg. All rights reserved.
//

import SwiftUI

struct MyCards: View {
    
    @State var showContent = false
    @State var showSettings = false
    
    @EnvironmentObject var viewModel: ListViewModel
    @EnvironmentObject var settings: UserSettings
    
    var body: some View {
        ZStack {
            NavigationView {
                List() {
                    ForEach(viewModel.items) { item in
                        NavigationLink(destination: NoteView(
                            title: item.title,
                            author: item.author,
                            date: item.date)) {
                                CardView(title: item.title, date: item.date, author: item.author, background: item.background)
                        }
                    }
                    .onDelete { index in
                        self.viewModel.items.remove(at: index.first!)
                    }
                    .onMove(perform: self.viewModel.move)
                }
                .navigationBarTitle(Text("Notes"))
                .navigationBarItems(
                    leading: Button(action: viewModel.addItem) {
                        Text("Add Item")},
                    trailing: EditButton()
                )
            }
            
            VStack() {
                Spacer()
                HStack {
                    Spacer()
                    Button(action: {
                        self.viewModel.togglePopUp()
                        //print("PRESSSS")
                    }) {
                        Image(systemName: "plus")
                    }
                    .padding(16)
                    .buttonStyle(ActionButtonStyle())
                }
            }
            PopUp()
        }
        
    }
    
    struct MyCards_Previews: PreviewProvider {
        static let viewModel = ListViewModel()
        static var previews: some View {
            MyCards().environmentObject(viewModel)
        }
    }
}
