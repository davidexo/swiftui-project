//
//  Courses.swift
//  SwiftUI-Project
//
//  Created by David Bielenberg on 11.01.20.
//  Copyright © 2020 David Bielenberg. All rights reserved.
//

import SwiftUI


struct Course: Identifiable {
    var id = UUID()
    var title: String
    var text: String
}

struct Courses: View {
    
    var courseViews = [
        Course(title: "Card 1", text: "ok"),
        Course(title: "Card 2", text: "ok"),
        Course(title: "Card 2", text: "ok"),
        Course(title: "Card 2", text: "ok"),
        Course(title: "Card 2", text: "ok"),
        Course(title: "Card 3", text: "ok")
    ]
    
    var body: some View {
        ScrollView(.horizontal, showsIndicators: false) {
            HStack(spacing: 10.0) {
                ForEach(courseViews) { item in
                    GeometryReader { geometry in
                        CourseView(title: item.title, text: item.text)
                            .rotation3DEffect(
                                Angle(degrees: Double(geometry.frame(in: .global).minX - 40) / -20),
                                axis: (x: 0.0, y: 10.0, z: 5.0)
                        )
                    }.frame(width: 200, height: 300)
                    
                    
                }
            }
            .padding(.leading, 32)
            .padding(.vertical, 40)
        }
    }
}

struct Courses_Previews: PreviewProvider {
    static var previews: some View {
        Courses()
    }
}

struct CourseView: View {
    
    let title: String
    let text: String
    
    var body: some View {
        VStack(alignment: .leading) {
            Text(title)
                .font(.title)
                .foregroundColor(Color.white)
            Text(text)
                .lineLimit(3)
                .lineSpacing(1.5)
                .foregroundColor(Color.white)
            Spacer()
        }
        .padding(16)
        .frame(width: 200, height: 300)
        .background(Color("blue"))
        .cornerRadius(16)
        .shadow(color: Color("shadow"), radius: 20.0, x: 0.0, y: 20)
    }
}
