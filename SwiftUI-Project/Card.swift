import SwiftUI

struct Card : Identifiable {
    var id = UUID()
    var title: String
    var date: String
    var author: String
    var background: Color
}


struct CardView: View {
    
    let title: String
    let date: String
    let author: String
    let background: Color
    
    @State private var isFav = false
    @State var favIconName = "heart"
    
    let accentColor: Color
    var iconSize = 40 as CGFloat
    
    
    init(title: String, date: String, author: String, background: Color) {
        self.title = title
        self.date = date
        self.author = author
        self.background = background
        
        accentColor = Color("blue")
    }
    
    func makeFavorite() {
        self.setIcon()
        isFav.toggle()
    }
    
    func setIcon() {
        if !isFav {
            favIconName = "heart.fill"
        } else {
            favIconName = "heart"
        }
    }
    
    var body: some View {
        VStack {
            HStack {
                Image(systemName: "star.fill")
                    .frame(width: iconSize, height: iconSize)
                    .foregroundColor(accentColor)
                    .background(accentColor)
                    .opacity(0.5)
                    .cornerRadius(iconSize/2)
                Text(title)
                    .font(.title)
                    .fontWeight(.medium)
                    .foregroundColor(.primary)
                Spacer()
                
                Button(action: {
                    self.makeFavorite()
                }) {
                    Image(systemName: favIconName)
                        .font(.title)
                }
            }
            HStack {
                Text("Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.")
                    .foregroundColor(Color("light-grey"))
                    .lineLimit(2)
                    .lineSpacing(4)
                Spacer()
            }
            HStack {
                Text("\(date), \(author)")
                    .foregroundColor(Color.blue)
                Spacer()
            }.padding(.vertical, 16)
        }
        .padding(.vertical, 16)
        .background(Color.clear)
        
    }
}
