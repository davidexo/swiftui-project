//
//  DetailView.swift
//  SwiftUI-Project
//
//  Created by David Bielenberg on 20.12.19.
//  Copyright © 2019 David Bielenberg. All rights reserved.
//

import SwiftUI

struct DetailView: View {
    var body: some View {
        ZStack {
            VStack {
                HStack {
                    Image(systemName: "star.fill")
                        .frame(width: /*@START_MENU_TOKEN@*/100.0/*@END_MENU_TOKEN@*/, height: /*@START_MENU_TOKEN@*/100.0/*@END_MENU_TOKEN@*/)
                        .foregroundColor(Color.red)
                        .background(/*@START_MENU_TOKEN@*/Color.blue/*@END_MENU_TOKEN@*/)
                        .cornerRadius(16)
                        .padding(16)
                    Text("User Name")
                    Spacer()
                    
                }
                .background(Color.green)
                .padding(16)
                
                InfoEntry()
                InfoEntry()
                InfoEntry()
                
                
                
                Spacer()
            }.background(Color.yellow)
        }.navigationBarTitle("Profile")
    }
}

struct DetailView_Previews: PreviewProvider {
    static var previews: some View {
        DetailView()
    }
}

struct InfoEntry: View {
    var body: some View {
        HStack {
            Image(systemName: "star.fill")
                .frame(width: 50.0, height: 50.0)
                .foregroundColor(Color.red)
                .background(/*@START_MENU_TOKEN@*/Color.blue/*@END_MENU_TOKEN@*/)
                .cornerRadius(16)
                .padding(16)
            Text("Info Cell")
            Spacer()
            
        }
        .background(Color.green)
        
    }
}
