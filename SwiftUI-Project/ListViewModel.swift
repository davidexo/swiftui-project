import SwiftUI
import Combine

class ListViewModel: ObservableObject {
    
    @Published var showPopUp: Bool = false
    
    @Published var items = [
        Card(title: "Card 1", date: "Heute", author: "David", background: Color.white),
        Card(title: "Card 2", date: "Heute", author: "David", background: Color.white),
        Card(title: "Card 3", date: "Heute", author: "David", background: Color.white)
    ]
    
    func addItem() {
        items.append(Card(title: "Card \(self.items.endIndex + 1)", date: "Heute", author: "David", background: Color.white))
        print(items.count)
    }
    
    func addYourIteam(card: Card) {
        items.append(Card(title: card.title, date: card.date, author: card.author, background: Color.white))
        print(items.count)
    }
    
    func togglePopUp() {
        showPopUp.toggle()
    }
    
    func move(source: IndexSet, destination: Int) {
        items.swapAt(source.first!, destination)
    }
}
