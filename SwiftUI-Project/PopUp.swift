//
//  PopUp.swift
//  SwiftUI-Project
//
//  Created by David Bielenberg on 27.12.19.
//  Copyright © 2019 David Bielenberg. All rights reserved.
//

import SwiftUI


struct PopUp: View {
    
    @State var note: String = ""
    @State var user: String = ""
    @State var title: String = ""
    
    
    @EnvironmentObject var viewModel: ListViewModel
    
    var body: some View {
        VStack {
            HStack {
                Text("Write a new note")
                    .font(.title)
                    .fontWeight(.bold)
                    .multilineTextAlignment(.leading)
                Spacer()
                Button(action: {
                    self.viewModel.togglePopUp()
                }) {
                    Image(systemName: "xmark.circle.fill")
                        .font(.title)
                }
                
            }
            TextField("Your note", text: $title)
                .textFieldStyle(RoundedBorderTextFieldStyle())
            TextField("Your name", text: $user)
                .textFieldStyle(RoundedBorderTextFieldStyle())
            Button(action: {
                self.createCard()
                self.viewModel.togglePopUp()
            }) {
                HStack{
                    Spacer()
                    Text("Hinzufügen")
                    Spacer()
                }
            }
            .buttonStyle(PrimaryButtonStyle())
            .padding(.top, 16)
        }
        .padding(16)
            .background(Color.white)
            .cornerRadius(16)
            .shadow(radius: 30)
            .padding(16)
            .opacity(viewModel.showPopUp ? 1 : 0)
            .offset(x: 0, y: viewModel.showPopUp ? 0 : 100)
            .animation(.easeInOut)
    }
    
    struct PopUp_Previews: PreviewProvider {
        static let viewModel = ListViewModel()
        
        static var previews: some View {
            NavigationView {
                PopUp().environmentObject(viewModel)
            }
        }
    }
    
    // MARK: FUNCTIONS
    
    func createCard() {
        let cardToAdd = Card(title: self.title, date: "Kot", author: self.user, background: Color.white)
        viewModel.addYourIteam(card: cardToAdd)
        print(viewModel.items.count)
    }
    
}
