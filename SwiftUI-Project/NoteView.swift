//
//  NoteView.swift
//  SwiftUI-Project
//
//  Created by David Bielenberg on 29.12.19.
//  Copyright © 2019 David Bielenberg. All rights reserved.
//

import SwiftUI

struct NoteView: View {
    
    var title = "Title"
    var text = "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet."
    var author = "Author"
    var date = "Date"
    var userImage = Image(systemName: "star")
    
    
    var body: some View {
        ZStack {
            HStack {
                VStack(alignment: .leading) {
                    HStack {
                        userImage
                            .frame(width: 40, height: 40)
                            .foregroundColor(Color("blue"))
                            .background(Color("light"))
                            .cornerRadius(20)
                        Text("\(author), \(date)")
                        
                    }
                    .foregroundColor(Color.blue)
                    .padding(.vertical, 16)
                    Text(text)
                        .padding(.vertical, 16)
                        .lineSpacing(/*@START_MENU_TOKEN@*/10.0/*@END_MENU_TOKEN@*/)
                        .lineLimit(10)
                        .opacity(0.7)
                    Spacer()
                    HStack {
                        EditButton()
                            .buttonStyle(PrimaryButtonStyle())
                        Button(action: /*@START_MENU_TOKEN@*/{}/*@END_MENU_TOKEN@*/) {
                            Image(systemName: "trash")
                        }.buttonStyle(PrimaryButtonStyle())
                    }
                }.padding(16)
                Spacer()
                
            }
        }.navigationBarTitle(title)
    }
}

struct NoteView_Previews: PreviewProvider {
    static var previews: some View {
        NoteView()
    }
}
